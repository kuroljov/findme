// @flow
'use strict'

import files from '../lib/files'

function home (x: Object, next: Function) {
  if (x.method !== 'GET' || x.url !== '/') {
    return next()
  }

  x.type = 'html'
  x.body = files.get('index.html')

  return x.body
}

export default home
