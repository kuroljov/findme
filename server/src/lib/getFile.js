// @flow
'use strict'

import fs from 'fs'
import path from 'path'

export default function getFile (name: string): Buffer | null {
  const file = path.resolve(__dirname, '../../../client', name)

  if (!fs.existsSync(file)) {
    return null
  }

  return fs.readFileSync(file)
}
