// @flow

export default function normalize (input: string): string {
  return input.trim().toLowerCase()
}
