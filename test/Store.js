// @flow
'use strict'

import t from 'tap'
import Store from '../server/src/lib/store/Store'

t.test('Store', t => {
  t.test('nothing here', t => {
    const store = new Store()
    const result = store.get('banana')

    t.ok(Array.isArray(result))
    t.equal(result.length, 0)
    t.end()
  })

  t.test('exact match', t => {
    const store = new Store()
    const me = { name: 'Viktor' }

    t.ok(store.set('me', me))

    const a = store.get('me')
    t.ok(Array.isArray(a))
    t.equal(a.length, 1)
    t.deepEqual(a, [me])
    t.end()
  })

  t.test('complete', t => {
    const store = new Store()

    const ban = 'ban'
    const banan = 'banan'
    const banen = 'banen'
    const bas = 'bas'
    const ba = 'ba'
    const bb = 'bb'
    const b = 'b'

    t.ok(store.set(b, { b }))
    t.ok(store.set(bb, { bb }))
    t.ok(store.set(ba, { ba }))
    t.ok(store.set(bas, { bas }))
    t.ok(store.set(ban, { ban }))
    t.ok(store.set(banan, { banan }))
    t.ok(store.set(banen, { banen }))

    const a = store.get('ba')
    t.ok(Array.isArray(a))
    t.ok(a.some(x => x.ba))
    t.ok(a.some(x => x.bas))
    t.ok(a.some(x => x.ban))
    t.ok(a.some(x => x.banan))
    t.ok(a.some(x => x.banen))
    t.equal(a.length, 5)
    t.end()
  })

  t.test('complete#2', t => {
    const store = new Store()

    const ban = 'ban'
    const banan = 'banan'
    const banen = 'banen'
    const bas = 'bas'

    t.ok(store.set(bas, { bas }))
    t.ok(store.set(ban, { ban }))
    t.ok(store.set(banan, { banan }))
    t.ok(store.set(banen, { banen }))

    const a = store.get('b')
    t.ok(Array.isArray(a))
    t.ok(a.some(x => x.bas))
    t.ok(a.some(x => x.ban))
    t.ok(a.some(x => x.banan))
    t.ok(a.some(x => x.banen))
    t.equal(a.length, 4)
    t.end()
  })

  t.test('toJSON', t => {
    const store = new Store()

    const a = 'a'
    const b = 'b'
    const c = 'c'
    const aa = 'aa'
    const bb = 'bb'
    const cc = 'cc'

    store.set(a, { a })
    store.set(b, { b })
    store.set(c, { c })
    store.set(aa, { aa })
    store.set(bb, { bb })
    store.set(cc, { cc })

    const json = store.toJSON()
    t.deepEqual(json, JSON.stringify({
      a: { a },
      b: { b },
      c: { c },
      aa: { aa },
      bb: { bb },
      cc: { cc }
    }))

    t.end()
  })

  t.test('reset', t => {
    const store = new Store()
    t.ok(store.set('a', { a: 1 }))
    t.equal(store.get('a')[0].a, 1)
    t.ok(store.reset())
    t.notOk(store.get('a').length)
    t.end()
  })

  t.end()
})
