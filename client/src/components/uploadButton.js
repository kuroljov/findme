// @flow

import { uploadButton } from '../lib/el'

const state = {
  listener: function none (e: Event) {}
}

function buttonClick (e: Event): void {
  state.listener(e)
}

function addEventListener (): void {
  uploadButton.addEventListener('click', buttonClick)
}

function removeEventListener (): void {
  uploadButton.removeEventListener('click', buttonClick)
}

function subscribe (listener: Function): void {
  state.listener = listener
}

const getButton = (): HTMLButtonElement => uploadButton

export default {
  subscribe,
  getButton,
  addEventListener,
  removeEventListener
}
