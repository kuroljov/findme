// @flow

import { searchField } from '../lib/el'

const state = {
  listener: function none (value) {}
}

function addEventListener (): void {
  searchField.addEventListener('keyup', function (e) {
    dispatch(this.value)
  })
}

function dispatch (value: string, overrideValue: boolean = false): void {
  if (overrideValue) {
    searchField.value = value
  }
  state.listener(value)
}

function subscribe (listener: Function): void {
  state.listener = listener
}

export default {
  dispatch,
  subscribe,
  addEventListener
}
