// @flow

function $ (id: string): Element {
  const el = document.getElementById(id)
  if (!el) {
    throw new Error(`#${id} is expected, got nothing`)
  }
  return el
}

export const cards = $('cards')
export const upload = $('upload')
export const search = $('search')
export const uploadButton = $('uploadButton')
export const uploadField = $('uploadField')
export const uploadLabel = $('uploadLabel')
export const searchField = $('searchField')
